# Managed by bumpversion
__version__ = "0.15.6"

from permifrost.error import SpecLoadingError

__all__ = [
    "SpecLoadingError",
]
